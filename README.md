The replication package for the ESEC/FSE 2017 submitted paper: "How Practitioners Perceive Coding Proficiency".
In the package, we have:

1. Technical Report of the paper: technical Report.pdf

2. Coding process for the 22 skills listed from Pages 5 to 8

3. Survey and Interview Questions: Developer Coding Proficiency.pdf

4. Raw Data from the survey participants: Raw Data-Global.pdf, and Raw Data-Chinese.pdf. Note we have a Chiese version of our survey

5. Scores for each skill: Results.xlxs

6. Data to reproduce the results in Tables 3-5 of the paper: ImportantTask-Table 3 &4.R, and experience-Table5.R